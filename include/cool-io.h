#ifndef COOL_IO_H
#define COOL_IO_H

//cool files include this header to use the stardard library's IO stream

#ifndef COOL_USE_OLD_HEADERS

#include<iostream>
using std :: ostream;
using std :: cout;
using std :: cerr;
using std :: endl;

#include<fstream>

using std :: ofstream;

#include<iomanip>

using std :: oct;
using std :: dec;
using std :: setw;
using std :: setfill;




#endif  //COOL_USE_OLD_HEADERS
#endif  //COOL_IO_H
