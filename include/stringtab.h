#ifndef _STRINGTAB_H_
#define _STRINGTAB_H_

#include<assert.h>
#include<string.h>
#include "list.h"
#include "cool-io.h"

class Entry;
typedef Entry* Symbol;

extern ostream& operator<<(ostream& s,const Entry& sym);
extern ostream& operator<<(ostream& s,Symbol sym);

//String Table Entries

class Entry{
protected:
    char* str;
    int len;
    int index;
public:
    Entry(char* s,int l,int i);

    int equal_string(char* s,int len) const;
    bool equal_index(int ind) const {return ind == index;};
    ostream& print(ostream& s) const;
    char* get_string() const;
    int get_len() const;
};

//There are three kinds of string table entries:
//1.a true string
//2.a string representation of an identifier
//3.a string representation of an integer
//
//code_def and code_ref are used by the code to produce definations and
//references(respectively) to constants.
//
class StringEntry : public Entry{
public:
    void code_def(ostream& str,int stringclasstag);
    void code_ref(ostream& str);
    StringEntry(char* s,int l,int i);
};

class IdEntry: public Entry{
public:
    IdEntry(char* s,int l,int i);
};

class IntEntry: public Entry{
public:
    void code_def(ostream& str,int intclasstag);
    void code_ref(ostream& str);
    IntEntry(char* s,int l,int i);
};

typedef StringEntry* StringEntryP;
typedef IdEntry* IdEntryP;
typedef IntEntry* IntEntryP;

template <class Elem>
class StringTable{
protected:
    List<Elem>* tbl;
    int index;
public:
    StringTable():tbl((List<Elem>* ) NULL),index(0) { }
    Elem* add_string(char* s,int maxchars);
    Elem* add_string(char* s);
    Elem* add_int(int i);

    //An iterator
    int first();
    int more(int i);
    int next(int i);

    Elem* lookup(int index);
    Elem* lookup_string(char* s);

    void print();
};

class IdTable : public StringTable<IdEntry>{};

class StrTable : public StringTable<StringEntry>{
public:
    void code_string_table(ostream&,int classtag);
};

class IntTable : public StringTable<IntEntry>{
public:
    void code_string_table(ostream&,int classtag);
};

extern IdTable idtable;
extern IntTable inttable;
extern StrTable stringtable;
#endif
