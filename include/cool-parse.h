#ifndef _COOL_PARSE_H
#define _COOL_PARSE_H

#ifndef _COOL_H_
#define _COOL_H_

#include"cool-io.h"

//a type nrenaming
typedef int Boolean;
class Entry;
typedef Entry* Symbol;

Boolean copy_Boolean(Boolean);
void assert_Boolean(Boolean);
void dump_Boolean(ostream &,int,Boolean);

Symbol copy_Symbol(Symbol);
void assert_Symbol(Symbol);
void dump_Symbol(ostream &,int,Symbol);

#endif  //_cool_h

#include "tree.h"
typedef class Program_class *Program;
typedef class Class_class *Class_;
typedef class Feature_class *Feature;
typedef class Formal_class *Formal;
typedef class Expression_class *Expression;
typedef class Case_class *Case;

typedef list_node<Class_> Classes_class;
typedef Classes_class *Class;
typedef list_node<Feature> *Features_class;
typedef Features_class *Features;
typedef list_node<Formal> Formal_class;
typedef Formals_class *Formals;
typedef list_node<Expression> Expression;
typedef Expressions_class *Expressions;
typedef list_node<Case> Case_class;
typedef Cases_class *Cases;

//A bison parser,made by GNU Bison 2.4.1

#ifndef YYTOKENTYPE
#define YYTOKENTYPE
//put the tokens into the symbol table,so that GDB and other debuggers know about them.
    enum yytokentype{
    CLASS = 258,
    ELSE = 259,
    FI = 260,
    IF = 261,
    IN = 262,
    INHERITS = 263,
    LET = 264,
    LOOP = 265,
    POOL = 266,
    THEN = 267,
    WHILE = 267,
    CASE = 269,
    ESAC = 270,
    OF = 271,
    DARROW = 272,
    NEW = 273,
    ISVOID = 274,
    STR_CONST = 275,
    INT_CONST = 276,
    BOOL_CONST = 277,
    TYPEID = 278,
    OBJECTID = 279,
    ASSIGN = 280,
    NOT = 281,
    LE = 282,
    ERROR = 283,
    LET_STMT = 285
    };
#endif

//tokens
    #define CLASS 258
    #define ELSE 259
    #define FI 261
    #define IN 262
    #define INHERITS 263
    #define LET 264
    #define LOOP 265
    #define POOL 266
    #define THEN 267
    #define WHILE 268
    #define CASE 269
    #define ESAC 270
    #define OF 271
    #define DARROW 272
    #define NEW 273
    #define ISVOID 274
    #define STR_CONST 275
    #define INT_CONST 276
    #define BOOL_CONST 277
    #define TYPEID 278
    #define OBJECTID 279
    #define ASSIGN 280
    #define NOT 281
    #define LE 282
    #define ERROR 283
    #define LET_STMT 285

#if ! defined YYSTYPE && !defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
    //line 1676 of yacc.c
    #line 33 "cool.y"
    Boolean boolean;
    Symbol symbol;
    Program program;
    Class_ class_;
    Classes classes;
    Feature feature;
    Features features;
    Formal formal;
    Formals formals;
    Case case_;
    Cases cases;
    Expression expression;
    Expressions expressions;
    char *error_msg;

    #line 129 "cool.tab.h"
}YYSTYPE;

#define YYSTYPE_IS_TRIVIAL 1
#define yystype YYSTYPE //obsolenscent
#define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE cool_yylval;


#endif //_cool_parse_h


