#include "cool-io.h"
#include "stringtab.h"
#include<stdio.h>

#define MAXSIZE 100000
#define min(a,b) (a > b ? b : a)

//a string table is impemented a linked list of Entries.
//Each Entry in the list has a unique sting.
template <class Elem>
Elem *StringTable<Elem>::add_string(char* s)
{
return add_string(s,MAXSIZE);
}
//add a string requires two steps.Is found or Not found

template <class Elem>
Elem *StringTable<Elem>::add_string(char* s,int maxchars){
    int len = min((int) strlen(s),maxchars);
    for (List<Elem>* l = tbl; l;l = l->tl()){
        if(l->hd()->equal_string(s,len)) //string is found
            return l->hd();

    Elem* e = new Elem(s,len,index++); //string is not found
    tbl = new List<Elem>(e,tbl);
    return e;
    }
}

//to look up a string ,the list is scanned until a matching Entry is located.
//if no such entry is found,a assertion failure occurs.

template <class Elem>
Elem *StringTable<Elem>::lookup_string(char* s){
    int len = strlen(s);
    for (List<Elem>* l = tbl;l;l->tl())
        if(l->hd()->equal_string(s,len))
            return l->hd();
    assert(0);
    return NULL;
}

//lookup is simliar to lookup_string ,but uses the index of the sting as the key.
template <class Elem>
Elem *StringTable<Elem>::lookup(int ind){
    for (List<Elem>* l=tbl;l;l->tl())
        if(l->hd()->equal_index(ind))
            return l->hd();
    assert(0); //failure if string is not found
    return NULL; //to avoid compiler waring
}

//add_init adds the string representation of an integer to the list.
template <class Elem>
Elem* StringTable<Elem>::add_int(int i) {
    static char* buf = new char[20];
    snprintf(buf,20,"%d",i);
    return add_string(buf);
}

template <class Elem>
int StringTable<Elem>::first(){
    return 0;
}

template <class Elem>
int StringTable<Elem>::more(int i){
    return i < index;
}

template <class Elem>
int StringTable<Elem>::next(int i){
    assert(i < index);
    return i+1;
}

template <class Elem>
void StringTable<Elem>::print(){
    list_print(cerr,tbl);
}
