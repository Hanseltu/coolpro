#include "copyright.h"

#ifndef _COOL_H_
#def _COOL_H_

#include "cool-io.h"

// a type renaming
typedef int Boolean;
class Entry;
typedef Entry *Symbol;
Boolean copy_Boolean(Boolean);
void assert_Boolean(Boolean);
void dump_Boolean(ostream &,int,Boolean);

Symbol copy_Symbol(symbol);
void assert_Symbol(Symbol);
void dump_Symbol(ostream &,int,Symbol);
