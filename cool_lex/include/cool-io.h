#include<copyright.h>

#ifndef COOL_IO_H
#define COOL_IO_H

/*
Cool files includes this header to use the standard library's IO streams.
*/

#ifndef COOL_USE_OLD_HEADERS

#include<iostream>

using std::ostream;
using std::cout;
using std::cerr;
using std::endl;

#include<fstream>

using std::ofsream;

#include<iomanip>

using std::oct;
using std::dec;
using std::setw;
using std::setfill;

/*
Including the entire std namespace doesn't work well because of conficts between e.g. std::plus and the plus AST node.
*/

/*Not Test wether the includers work
#else
#include<iostream.h>
#include<fstream.h>
#include<iomanip.h>
*/
#endif //COOL_USE_OLD_HEADERS

#endif //COOL_IO_H
