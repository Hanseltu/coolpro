#define _STRINGTAB_H_

#include <assert.h>
#include <string.h>
#include "list.h"
#include "cool-io.h"

class Entry;
typedef Entry * Symbol;

extern ostream& operator << (ostream& s,const Entry& sym);
extern ostream& operator << (ostream& s,const Symbol sym);

class Entry{
protected:
	char *str;
	int len;
	int index;
public:
	Entry(char *s,int l,int i);
	int equal_string(char *s,int len) const;
	bool equal_index(int ind) const {return ind == index;}
	ostream& print(ostream& s) const;

	char *get_string() const;
	int get_len() const;
};

class StringEntry : public Entry {
public:
	void code_def(ostream& str,int stringclasstag);
	void code_ref(ostream& str);
	StringEntry(char *s, int l, int i);
};

class IdEntry : public Entry{
public:
	IdEntry(char *s,int l,int i);
};

class IntEntry : public Entry{
public:
	void code_def(ostream& str,int intclasstag);
	void code_ref(ostream& str);
	IntEntry(char *s, int l,int i);
	};

	typedef StringEntry *StringEntryP;
	typedef IdEntry *IdEntryP;
	typedef IntEntry *IntEntryP;


	//String Tables

template <class Elem>
class StringTable{
protected:
	List<Elem>*tbl;
	int index;
public:
	StringTable():tbl((List<Elem> *) NULL),index(0){} //an empty table
	Elem *add_string(char *s,int maxchars);
	}
