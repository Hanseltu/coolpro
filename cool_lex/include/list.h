#include "copyright.h"

#ifndef _LIST_H_
#define _LIST_H_

#include "cool-io.h"
#include <stdlib.h>

template <class T>
class List{
private:
	T *head;
	List<T>* tail;
public:
	List(T *h,List<T> *t = NULL):head(h),tail(t){ }

	T *hd() const {return head;}
	List<T>* tl() const {return tail;}
};

template <class T>
void list_map(void f(*T),List<T>*l){
	for (l; l != NULL; l = l ->tl())
		f(l->hd());
}

template <class S, class T>
void list_print(S &str,List<T> *l){
	str << "[\n";
	for (; l != NULL; l = l->tl())
		str << *(l->hd()) << " ";
	str << "]\n";
}

template <class T>
int list_length(List<T>*l){
	int i = 0;
	for (; l != NULL; l = l->tl())
		i++;
	return i;
}

#endif
