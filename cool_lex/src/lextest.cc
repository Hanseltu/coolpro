/*
see copyright.h for copyright notice and limitation of liability and disclaimer of warranty provisions.
*/

#include"copyright.h"

/*
Name:lextest.cc
Function:reads input from file argument
Author:Hanseltu
Date:12/4/2017
*/

#include<stdio.h>
#include<unistd.h>
#include "cool-parse.h"
#include "utilities.h"

//global variable to date with the line number of the current line read from the input
int curr_lineno = 1;
char *curr_filename = "<stdin>";
FILE *fin;


extern int cool_yylex(); //function produced by flex.it returns the next token each time it is called.
YYSTYPE cool_yylval; //not compiler with parser,so must define this.

extern int optind; //used for option processing debug???

extern int yy_flex_debug;
extern int lex_verbose;
void handle_flags(int argc,char *argv[]);

int cool_yydebug;	//not used by the lexer but is needed to link with handle_flags

//define in utulities,cc
extern void dump_cool_token(ostream &out,int lineno,int token,YYSTYPE yylval);

int main(int argc,char**argv){
	int token;

	handle_flags(argc,argv);
	while(optind < argc){
				fin = fopen(argv[optind],"r");
				if (fin == NULL) {
			 cerr << "Could not open input file"<< argv[optind] << endl;
				exit(1);
		  }
			 
				curr_lineno = 1;
				count << "#name \"" << argv[optind] << "\"" << endl;
				while ((token = cool_yylex()) != 0){
					dump_cool_token(cout,curr_lineno,token,cool_yylval);
				}
				fclose(fin);
				optind++；
	}	
	exit(0);
}
