#include "copyright.h"

#include <stdio.h>
#include<stdlib.h>
#include "cool-io.h"
#include "unistd.h"
#include "cgen_gc.h"

extern int yy_flex_debug;
extern int cool_yydebug;

int lex_verbose;
int semant_debug;
int cfen_debug;
bool disable_reg_alloc;

int cgen_optimize;
char *out_filename;
Memmgr cgen_Memmgr = GC_NOGC;
Memmgr_Test cgen_Memmgr_Test = GC_NORMAL;
Memmgr_Debug cgen_Memmgr_Debug = GC_QUICK;

extern int optid;
extern char *optarg;

void handle_flags(int argc,char *argv[]){
	int c;
	int unknowopt = 0;

	//no debugging or optimization by default
yy_flex_debug = 0;
cool_yydebug = 0;
lex_verbose = 0;
semant_debug = 0;
cgen_debug = 0；
cgen_optimize = 0;
disable_reg_alloc = 0;

while ((c = getopt(argc,argv,"lpscvrOo:gtT")) != -1){
	switch (c){
#ifdef DEBUG
	case '1' : yy_flex_debug = 1;break;
	case 'p' : cool_yydebug = 1; break;
	case 's' : semant_debug = 1; break;
	case 'c' : cgen_debug = 1;   break;
	case 'v' : lex_verbose = 1;  break;
	case 'r' : disable_reg_alloc = 1; break;
#else
	case '1':
	case 'p':
	case 's':
	case 'c':
	case 'v':
	case 'r':
	 cerr << "No debugging aviailable\n";break;
#endif
	case 'g' : cgen_Memmgr = GC_GENGC;break;
	case 't' : cgen_Memmgr_Test = GC_TEST;break;
	case 'T' : cgen_Memmgr_Debug = GC_DEBUG;break;
	case 'o' : out_filename = optarg;break;
	case 'O' : cgen_optimize = 1;break;
	case '?' : unknowopt = 1;break;
	case ':' : unknowopt = 1;break;
	}
}
	if (unknowopt) {
		cerr << "usage: " << argv[0] << 
#ifdef DEBUG
	"[-lvpscOgtTr -o outname ][input-files]\n"
#else
	"[-OgtT -o outname][input-files]\n"
#endif
	exit(1);
	}
}
