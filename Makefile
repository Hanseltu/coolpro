COOL_DIR = ~/Documents/CompilerPro/coolpro

SCRIPT_SHELL = /bin/bash


#CLASS = cs143

LIB = -lfl

GMAKE = gmake

AR = gar

ARCHIVE_NEW = -cr
RANLIB = gar -qs

BINARIES = spim xspim aps2c++ coolc
#ASSIGNMENTS

ARCH = echo i686

DISPATCH_SCRIPTS = ${BINARIES:%=bin/%}
LINKAGE_SCRIPTS = etc/link-object etc/link-shared etc/copy-skel

force: 
install: ${DISPATCH_SCRIPTS} ${LINKAGE_SCRIPTS} ${ASSIGNMENTS:%=${COOL_DIR}/assignments/%/Makefile}

#
# assignments/${ASSIGNMENTS}/Makefile <== assignments/${ASSIGNMENTS}/Makefile.SKEL
#
${patsubst %,${COOL_DIR}/assignments/%/Makefile,${ASSIGNMENTS}}: %: %.SKEL Makefile force
	rm -f temp; \
	sed -e 's#CLASSDIR= ?#CLASSDIR= ${COOL_DIR}#g'  \
	    -e 's#CLASS= ?#CLASS= ${CLASS}#g'           \
	    -e 's#AR= ?#AR= ${AR}#g'			\
	    -e 's#ARCHIVE_NEW= ?#ARCHIVE_NEW= ${ARCHIVE_NEW}#g' \
	    -e 's#RANLIB= ?#RANLIB= ${RANLIB}#g' \
	    -e 's#LIB= ?#LIB= ${LIB}#g'  $< > temp;     \
	mv -f temp $@; chmod ugo+r $@

#
# bin/${BINARIES} <== bin/dispatch.SKEL
#
${DISPATCH_SCRIPTS}: ${COOL_DIR}/bin/dispatch.SKEL force
	sed -e 's#SHELL#${SCRIPT_SHELL}#g' \
	    -e 's#DIR#${COOL_DIR}#g' \
	    -e 's#ARCH#${ARCH}#g' \
	    -e 's#PROGRAM#${patsubst bin/%,%,$@}#g' $< > $@
	chmod ugo+rx $@

#
# ${LINKAGE_SCRIPTS} <== ${LINKAGE_SCRIPTS}.SKEL
#
${LINKAGE_SCRIPTS}: %: %.SKEL force
	sed -e 's#SHELL#${SCRIPT_SHELL}#g' \
	    -e 's#ARCH#${ARCH}#g' \
	    -e 's#DIR#${COOL_DIR}#g' $< > $@
	chmod ugo+rx $@




 
